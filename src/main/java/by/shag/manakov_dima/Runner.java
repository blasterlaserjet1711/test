package by.shag.manakov_dima;

import by.shag.manakov_dima.jpa.RobotShop;
import by.shag.manakov_dima.jpa.T1000;
import by.shag.manakov_dima.jpa.Terminator;
import org.springframework.beans.BeansException;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

@SpringBootApplication
public class Runner implements ApplicationContextAware {

    private static ApplicationContext context;

    public static void main(String[] args) {
        SpringApplication.run(Runner.class, args);
        System.out.println("!!!!!!!!!!");
        Object obj = context.getBean("runner");
//        T1000 robot =  context.getBean(T1000.class);
//        robot.sayName();
//        Terminator terminator = context.getBean(Terminator.class);
//        terminator.sayName();
        RobotShop robotShop = context.getBean(RobotShop.class);
        robotShop.show("T-1000");
        robotShop.show("terminator");
        System.out.println("!!!!!!!!");



    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        context = applicationContext;

    }
}
