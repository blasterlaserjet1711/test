package by.shag.manakov_dima.jpa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

//@Component
public class RobotShop {

    private String name;

    public void setName(String name) {
        this.name = name;
    }

    @Autowired
    private T1000 t1000;

   @Autowired
    private Terminator terminator;

    public void show(String name) {
        if ("T-1000".equals(name)) {
            t1000.sayName();
        }else {
            terminator.sayName();
        }
    }

}
