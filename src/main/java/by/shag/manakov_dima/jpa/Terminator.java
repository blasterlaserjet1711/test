package by.shag.manakov_dima.jpa;

import org.springframework.stereotype.Component;

@Component
public class Terminator {
    private String name = "Terminator";

    public void sayName() {
        System.out.println("I am " + name);
    }

}
