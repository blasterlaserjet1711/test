package by.shag.manakov_dima.jpa;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Service

public class T1000 {
    private String name = "T-1000";

    public void sayName() {
        System.out.println("I am " + name);
    }
}
