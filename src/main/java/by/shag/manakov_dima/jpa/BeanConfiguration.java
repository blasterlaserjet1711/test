package by.shag.manakov_dima.jpa;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

@Configuration
public class BeanConfiguration {
    @Bean
    public RobotShop robotShop1() {
       RobotShop shop = new RobotShop();
       shop.setName("magaz");
       return shop;
    }

   @Primary
    @Bean
    public RobotShop robotShop2() {
        RobotShop shop = new RobotShop();
        shop.setName("butic");
        return shop;
    }
}
